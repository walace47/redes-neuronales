const request = require('request');
const util = require('./funciones/funciones.js');
var Perceptron = require('./modelo/Perceptron');
var synaptic = require('synaptic');
const math = require('mathjs');

let p = new Perceptron(2);

const actFunc = (sum) => {
        if (sum >= 0) {
            return 1;
        } else {
            return -1;
        }
    }
    //Resuelve el dos parametros 1 y el 8 
    //Resuelve el 6 con 0.00000000001; con parametro
    //el 13 demora mucho y queda cerca de la respuesta
let dataSet = util.obtenerDataSet(1);
let par = [];
console.log(dataSet);
for (let i = 0; i < dataSet.length; i++) {
    let parameters = dataSet[i].ParametrosEntrada.split(",");
    par.push({
        // inputs: [dataSet[i].ParametrosEntrada.replace(",", ".")],
        inputs: [parseInt(parameters[0]), parseInt(parameters[1])],
        output: [dataSet[i].Salida]
    });
}
//console.log(par);
par = util.normalizar(par);

let normalizador = par[0].normalizador;
console.log(par);

let res = 0;
let err = -10000;
let errTmp = 100;
let i = 1;
while (errTmp != 0) {
    console.log(errTmp.toString());
    err = errTmp;
    //while( err !==0){

    errTmp = math.bignumber(0);
    for (let i = 0; i < par.length; i++) {

        errTmp = math.add(errTmp, p.train(par[i].inputs, par[i].output));

    }
    //errTmp = errTmp[0].value;
    //console.log(errTmp);
    //errTmp = math.round(errTmp);
    //errTmp = errTmp.toString()
    if (math.bignumber(errTmp) < math.abs(math.bignumber('1e-6'))) {
        errTmp = math.round(errTmp);
    }
    // console.log(p);
    //}
    /* if (i * 100 === res) {
         let c = p.calculo([math.divide(4, normalizador), math.divide(5, normalizador)]);
         c = math.multiply(c, normalizador);
         console.log(math.round(c));
         i++;
     }*/

    res++;
}
//p.train([1,2],3);
//{ inputs: [ 4, 5 ], output: [ 95 ] },
// { inputs: [ 4, 6 ], output: [ 96 ] },
//  { inputs: [ 4, 7 ], output: [ 97 ] },
//  { inputs: [ 4, 8 ], output: [ 98 ] },
//  { inputs: [ 4, 9 ], output: [ 99 ] },
// { inputs: [ 4, 10 ], output: [ 100 ] }
console.log(`paso ${res} epocas`);
console.log(p)
let c = p.calculo([math.divide(7710, normalizador), math.divide(8527, normalizador)]);
c = math.multiply(c, normalizador);
let final = p.calculo([5, 15]);
let final1 = p.calculo([20, 30]);
let final2 = p.calculo([6, 10]);
let final3 = p.calculo([4, 170]);
final = math.round(c);
final1 = math.round(c);
final2 = math.round(c);
final3 = math.round(c);
console.log(final.toString());
console.log(final.toString());
console.log(c.toString());
//console.log(final2.toString());
//console.log(final3.toString());

/*
{ idSolucion: 484,
  idProblema: 3,
  ParametrosEntrada: '410,3090',
  Salida: 29468139.09 },
{ idSolucion: 485,
  idProblema: 3,
  ParametrosEntrada: '7710,8527',
  Salida: 41675202.61 },*/