let Dendrita = require('../modelo/Dendrita');
module.exports = class Neurona{
  constructor(dendritas){
    this.dendritas = dendritas;
    this.umbral = new Dendrita(1,1);
  }
  calcular(){
    let salida = 0;
    for (let i = 0; i < this.dendritas.length;i++){
      salida+= this.dendritas[i].valor * this.dendritas[i].peso;
    }
    salida+= this.umbral.peso * this.umbral.valor;
    return salida;
  }
}
