const math = require('mathjs');
const util = require('../funciones/funciones');
const sigmoide = (x) => {
    return 1 / (1 + Math.exp(-x));
}

const derivadaSigmoide = (y) => {
    return y * (1 - y);
}
const funcionActivacionIdentidad = (sum) => {
    return sum;
}
const escalonada = (sum) => {
    if(sum >= 1) return 1
    else return 0;
}

math.config({
    number: 'BigNumber', // Default type of number:
    // 'number' (default), 'BigNumber', or 'Fraction'
    precision: 20 // Number of significant digits for BigNumbers
})

module.exports = class RedNeuronal {
    constructor(inputLayer, hidenLayer, outputLayer) {
        this.inputLayer = inputLayer;
        this.hidenLayer = hidenLayer;
        this.outputLayer = outputLayer;

        this.pesosInputsHiden = math.matrix(util.construirMatrizAleatoria(this.hidenLayer, this.inputLayer));
        this.pesosHidenOutput = math.matrix(util.construirMatrizAleatoria(this.outputLayer, this.hidenLayer));
        this.learningRate = 0.1;

        this.biasH = math.matrix(util.construirMatrizAleatoria(this.hidenLayer, 1));
        this.biasO = math.matrix(util.construirMatrizAleatoria(this.outputLayer, 1));

        console.log(this.pesosInputsHiden);
        console.log(this.pesosHidenOutput);
        console.log(this.biasH)
        console.log(this.biasO)

    }
    feedforward(inputs) {
        let inputAux = [];
        inputs.forEach(element => {
            inputAux.push([element]);
        });
        //calculo la salida de la capa del medio
        let outputHidden = math.multiply(this.pesosInputsHiden, math.matrix(inputAux));
        outputHidden = math.add(outputHidden, this.biasH);
        //funcion de activacion
        outputHidden = math.map(outputHidden, (value) => {
            return sigmoide(value);
        });
        //calculo la salida de la ultima capa
        let outputFinalLayer = math.multiply(this.pesosHidenOutput, outputHidden);
        outputFinalLayer = math.add(outputFinalLayer, this.biasO);
        //funcion de activacion
        outputFinalLayer = math.map(outputFinalLayer, (value) => {
            return escalonada(value);
        });
        return outputFinalLayer;
    }

    train(inputs, targets) {
        let targetsAux = [];
        targets.forEach(element => {
            targetsAux.push([element]);
        });
        targetsAux = math.matrix(targetsAux);

        // el metodo feedforward se necesitaban algunas variables

        let inputAux = [];
        inputs.forEach(element => {
            inputAux.push([element]);
        });
        //calculo la salida de la capa del medio
        let outputHidden = math.multiply(this.pesosInputsHiden, math.matrix(inputAux));
        outputHidden = math.add(outputHidden, this.biasH);
        //funcion de activacion
        outputHidden = math.map(outputHidden, (value) => {
            return sigmoide(value);
        });
        //calculo la salida de la ultima capa
        let outputFinalLayer = math.multiply(this.pesosHidenOutput, outputHidden);
        outputFinalLayer = math.add(outputFinalLayer, this.biasO);
        //funcion de activacion
        outputFinalLayer = math.map(outputFinalLayer, (value) => {
            return sigmoide(value);
        });

        //calculo el error global
        let outputsError = math.subtract(targetsAux, outputFinalLayer);
        let gradiente = math.map(outputFinalLayer, (value) => derivadaSigmoide(value));

        gradiente = math.dotMultiply(gradiente, outputsError);
        gradiente = math.dotMultiply(gradiente, this.learningRate);

        //ajusto segun el gradiente
        let outputHiddenTrans = math.transpose(outputHidden);

        //console.log(outputHiddenTrans);
        //console.log(gradiente);
        let pesosHiddenOutDelta = math.multiply(gradiente, outputHiddenTrans);
        this.pesosHidenOutput = math.add(this.pesosHidenOutput, pesosHiddenOutDelta);

        //calculo del error capa escondida
        let pesosHidenOutputTrans = math.transpose(this.pesosHidenOutput);
        let hiddenErrors = math.multiply(pesosHidenOutputTrans, outputsError);


        //calculo el gradiente de la capa escondida
        let hiddenGradient = math.map(outputHidden, (value) => derivadaSigmoide(value));

        hiddenGradient = math.dotMultiply(hiddenGradient, hiddenErrors);
        hiddenGradient = math.dotMultiply(hiddenGradient, this.learningRate);

        //ajusto segun el gradiente
        let inputTrans = math.transpose(inputAux);
        let pesosInputsHidenDelta = math.multiply(hiddenGradient, inputTrans);
        this.pesosInputsHiden = math.add(this.pesosInputsHiden,pesosInputsHidenDelta);
        return hiddenErrors;

    }
}
