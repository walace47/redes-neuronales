const util = require('../funciones/funciones');
const math = require('mathjs');
math.config({
    number: 'BigNumber', // Default type of number:
    precision: 20 // Number of significant digits for BigNumbers
})
const funcionActivacionEscalonada = (sum) => {
    let res;
    if (sum > 0) {
        res = 1;
    } else {
        res = 0
    }
    return res;
}
const funcionActivacionSigmoide = (x) => {
    return math.divide(math.bignumber(1), math.add(math.bignumber(1), math.bignumber(Math.exp(-x))));
}
const funcionActivacionIdentidad = (sum) => {
    return sum;
}
module.exports = class Perceptron {
    constructor(f) {
        //this.defaultActivatedFunction = funcionActivacionIdentidad;
        this.pesos = [];
        this.b = math.bignumber(util.aleatorio());
        this.umbral = math.bignumber(1);
        for (let j = 0; j < f; j++) {
            this.pesos.push(math.bignumber(util.aleatorio()));
        }
    }

    //Recibe por parametro una funcion de activacion
    calculo(inputs, activatedFunction = funcionActivacionIdentidad) {
            //console.log(this.b)
            let suma = math.add(this.b, 0);

            for (let i = 0; i < this.pesos.length; i++) {
                let sumatemp = suma;
                suma = math.add(suma, math.multiply(math.bignumber(inputs[i]), this.pesos[i]));
                //suma += inputs[i] * this.pesos[i];
                //console.log(`${suma} e = ${sumatemp} + ${inputs[i]} * ${this.pesos[i]}`);

            }
            return funcionActivacionIdentidad(suma);
        }
        //Recibe por parametro la funcion de activacion
    train(inputs, target, activatedFunction = funcionActivacionIdentidad) {
        let entrenamiento, error;
        entrenamiento = this.calculo(inputs);
        error = math.multiply((math.subtract(math.bignumber(target), entrenamiento)), this.umbral);
        for (let i = 0; i < this.pesos.length; i++) {
            this.pesos[i] = math.add(this.pesos[i], math.multiply(error, math.bignumber(inputs[i])));
            //error * inputs[i];
        }

        this.b = math.add(this.b, error);


        return error;
    }

    random() {
        let numeroRandom = Math.random();
        if (numeroRandom >= 0.5) {
            return numeroRandom * -1;
        } else {
            return numeroRandom;
        }
    }

}