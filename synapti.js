var synaptic = require('synaptic');
const util = require('./funciones/funciones.js');

let dataSet = util.obtenerDataSet(1);

var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

var inputLayer = new Layer(2);
var hiddenLayer = new Layer(3);
var outputLayer = new Layer(1);

inputLayer.project(hiddenLayer);
hiddenLayer.project(outputLayer);

let par = [];
for (let i = 0; i < dataSet.length; i++) {
    let parameters = dataSet[i].ParametrosEntrada.split(",");
    par.push({
        // inputs: [dataSet[i].ParametrosEntrada.replace(",", ".")],
        input: [parseInt(parameters[0]), parseInt(parameters[1])],
        output: [dataSet[i].Salida]
    });
}
console.log(par);
var myNetwork = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
});
var trainer = new Trainer(myNetwork)


trainer.train(par, {
    cost: Trainer.cost.IDENTITY
});

console.log(myNetwork.activate([100, 2]));