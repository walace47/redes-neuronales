const fs = require('fs');
const util = require('./funciones/funciones.js');
let par = [],
    par2 = [],
    par3 = [];
par4 = [];
par5 = [];
par6 = [];


for (let i = 1; i < 42; i++) {
    console.log("iteraacion: " +
        i)
    let dataSet = [];
    dataSet = util.obtenerDataSet(i);
    if (dataSet.length >= 1) {
        let entrada = dataSet[0].ParametrosEntrada.toString();
        let longitud = (entrada.match(/,/g) || []).length
        console.log(entrada);

        if (longitud === 0) {

            for (let j = 0; j < dataSet.length; j++) {
                par.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [parseInt(dataSet[j].ParametrosEntrada)],
                    output: [dataSet[j].Salida]
                });
            }


        } else if (longitud === 1) {
            for (let j = 0; j < dataSet.length; j++) {
                let parameters = dataSet[j].ParametrosEntrada.split(",");
                par2.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [parseInt(parameters[0]), parseInt(parameters[1])],
                    output: [dataSet[j].Salida]
                });
            }



        } else if (longitud === 2) {
            for (let j = 0; j < dataSet.length; j++) {

                let parameters = dataSet[j].ParametrosEntrada.split(",");
                par3.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [parseInt(parameters[0]), parseInt(parameters[1]), parseInt(parameters[2])],
                    output: [dataSet[j].Salida]
                });
            }

        } else if (longitud === 3) {
            for (let j = 0; j < dataSet.length; j++) {

                let parameters = dataSet[j].ParametrosEntrada.split(",");
                par4.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [parseInt(parameters[0]), parseInt(parameters[1]), parseInt(parameters[2]), parseInt(parameters[3])],
                    output: [dataSet[j].Salida]
                });
            }

        } else if (longitud === 4) {
            for (let j = 0; j < dataSet.length; j++) {

                let parameters = dataSet[j].ParametrosEntrada.split(",");
                par5.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [
                        parseInt(parameters[0]),
                        parseInt(parameters[1]),
                        parseInt(parameters[2]),
                        parseInt(parameters[3]),
                        parseInt(parameters[4])
                    ],
                    output: [dataSet[j].Salida]
                });
            }

        } else if (longitud === 5) {
            for (let j = 0; j < dataSet.length; j++) {

                let parameters = dataSet[j].ParametrosEntrada.split(",");
                par6.push({
                    idProblema: dataSet[j].idProblema,
                    idSolucion: dataSet[j].idSolucion,
                    inputs: [
                        parseInt(parameters[0]),
                        parseInt(parameters[1]),
                        parseInt(parameters[2]),
                        parseInt(parameters[3]),
                        parseInt(parameters[4]),
                        parseInt(parameters[5]),

                    ],
                    output: [dataSet[j].Salida]
                });
            }

        }


    }
}




fs.appendFileSync('./tresParametro.json', JSON.stringify(par3));
fs.appendFileSync('./dosParametro.json', JSON.stringify(par2));
fs.appendFileSync('./unParametro.json', JSON.stringify(par))
fs.appendFileSync('./cuatroParametro.json', JSON.stringify(par4))
fs.appendFileSync('./cincoParametro.json', JSON.stringify(par5))
fs.appendFileSync('./seisParametro.json', JSON.stringify(par6))