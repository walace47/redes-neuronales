let Dendrita = require('./modelo/Dendrita');
let Neurona = require('./modelo/Neurona');
const obtenerDataSet = require('./funciones/funciones.js')

let entrada1 = new Dendrita(0.5,0);
let entrada2 = new Dendrita(0.5,0);

let neurona1 = new Neurona([entrada1,entrada2]);
let res = -1;
//obtengo el data set del problema 1
let dataSet = obtenerDataSet(1);
let epocas = 0;
//console.log(dataSet);
console.log(contarDigitos(100));
while (res !== 0){
  res = 0;
  console.log(neurona1);
  for (let i = 0; i < dataSet.length; i++){
    let par = dataSet[i].ParametrosEntrada.split(",");
    neurona1.dendritas[0].valor = par[0];
    neurona1.dendritas[1].valor = par[1];
    let salidaNeurona = neurona1.calcular();
  //  console.log(neurona1);
//    console.log("La respuesta es:" + salidaNeurona);
//    console.log("La respuesta deberia ser: "+dataSet[i].Salida );
    let error = salidaNeurona - dataSet[i].Salida;
    //console.log("El error es: "+error);
    res +=error;
    let temp1,temp2;
    temp1 = error*neurona1.dendritas[0].peso;
    temp2 = error*neurona1.dendritas[1].peso;
  //  console.log(temp1)
  //  console.log(temp2)

    neurona1.dendritas[0].peso = neurona1.dendritas[0].peso + contarDigitos(temp1);
    neurona1.dendritas[1].peso = neurona1.dendritas[1].peso + contarDigitos(temp2);

    neurona1.umbral.peso = contarDigitos(neurona1.umbral.peso + error);
  }
  epocas++;
}

function contarDigitos(numero){
  let i = 1;
  let temp = numero;
  if (temp < 0){
    temp = temp*-1
  }
  while (temp > 1){
    temp = temp / 10;
    i++;
  }
  let p = Math.pow(10,i);
  //console.log("los parametros son "+ numero +" : "+ p);

  return numero/p
}
