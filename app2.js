var RedNeuronal = require('./modelo/Red');

let brain = new RedNeuronal(2, 4, 1);
//console.log(brain);
let e = brain.feedforward([500, 200]);
//e = brain.train([0, 1], [1]);


let training_data = [{
    inputs: [0, 0],
    outputs: [0]
  },
  {
    inputs: [1, 1],
    outputs: [1]
  },
  {
    inputs: [1, 0],
    outputs: [0]
  },
  {
    inputs: [0, 1],
    outputs: [0]
  }
];

for (let i = 0; i < 30000; i++) {
  let data = training_data[i%4];
  brain.train(data.inputs, data.outputs);
}

console.log(brain.feedforward([1,0]));
console.log(brain.feedforward([0,1]));
console.log(brain.feedforward([1,1]));
console.log(brain.feedforward([0,0]));
