const fs = require('fs');
var math = require('mathjs');
math.config({
    number: 'number', // Default type of number:
    precision: 64 // Number of significant digits for BigNumbers
})
const aleatorio = () => {
    let numeroRandom = Math.random();
    if (numeroRandom >= 0.5) {
        return numeroRandom * -1;
    } else {
        return numeroRandom;
    }
}

const normalizar = (datos) => {
    console.log(datos)
    let salida = [];
    let mayor = [];
    let mayorOut = [];
    let normalizador = 1;
    datos.forEach(dato => {
        dato.inputs.forEach(input => {
            if (normalizador < Math.abs(input)) normalizador = Math.abs(input);
        });
        dato.output.forEach(output => {
            if (normalizador < Math.abs(output)) normalizador = Math.abs(output);
        });

    });
    normalizador = math.multiply(normalizador, 1);
    let nuevoSet = [];
    datos.forEach(dato => {
        let nuevoInput = [];
        dato.inputs.forEach(input => nuevoInput.push(math.bignumber(input / math.bignumber(normalizador))));
        let nuevoOutput = [];
        dato.output.forEach(output => nuevoOutput.push(math.bignumber(output / math.bignumber(normalizador))));
        nuevoSet.push({
            inputs: nuevoInput,
            output: nuevoOutput,
            normalizador
        });
    });



    return nuevoSet;
}
const obtenerDataSetProblema = (numero) => {
    const contentBuffer = fs.readFileSync("Solucion.json");
    const contenido = JSON.parse(contentBuffer.toString());
    let respuesta = [];
    let parar = false;
    let i = 0;
    while (!parar && i <= contenido.length) {
        if (contenido[i].idProblema === numero) {
            respuesta.push(contenido[i]);
        }
        if (contenido[i].idProblema > numero) {
            parar = true;
        }
        i++;
    }
    return respuesta;
}

const construirMatrizAleatoria = (rows, cols) => {
    let matrix = [];
    for (let i = 0; i < rows; i++) {
        matrix[i] = [];
        for (let j = 0; j < cols; j++) {
            matrix[i][j] = aleatorio();
        }

    }
    return matrix;
}

const construirMatriz = (rows, cols) => {
    let matrix = [];
    for (let i = 0; i < rows; i++) {
        matrix[i] = [];
        for (let j = 0; j < cols; j++) {
            matrix[i][j] = aleatorio();
        }

    }
    return matrix;
}
module.exports = {
    obtenerDataSet: obtenerDataSetProblema,
    aleatorio,
    normalizar,
    construirMatriz,
    construirMatrizAleatoria
};